package com.infinit.cvdb.repository;

import com.infinit.cvdb.domain.Resume;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Extension interface to allow special queries
 *
 * Created by owahlen on 17.04.14.
 */
public interface ResumeRepositoryCustom {

	/**
	 * Use a search string to find a result page of resumes
	 * @param search string to be used for the lookup
	 * @param pageable page and sort order to be returned
	 * @return Page of Resume instances
	 */
	Page<Resume> search(String search, Pageable pageable);

}
