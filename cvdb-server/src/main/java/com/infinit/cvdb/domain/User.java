package com.infinit.cvdb.domain;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

/**
 * Representation of a user that is allowed to log into the system.
 *
 * Created by owahlen on 31.12.13.
 */
@Entity
public class User {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false, unique = true)
	private String username;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private boolean enabled;

	@ManyToMany(fetch=FetchType.EAGER)
	private Collection<Role> roles;

	// Getters and Setters
	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

    public void addToRoles(Role role) {
        if(roles==null) {
            roles = new HashSet<Role>();
        }
        roles.add(role);
    }

    public void removeFromRoles(Role role) {
        roles.remove(role);
    }

	@Override
	public String toString() {
		return getUsername();
	}

}
