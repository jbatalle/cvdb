package com.infinit.cvdb.test

import com.infinit.cvdb.config.TestApplication
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.mock.web.MockHttpSession
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.FilterChainProxy
import org.springframework.security.web.context.HttpSessionSecurityContextRepository
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup

/**
 * Base class for integration tests providing a
 * {@link org.springframework.web.context.WebApplicationContext WebApplicationContext}
 * Created by owahlen on 29.07.14.
 */
@ContextConfiguration(classes = [TestApplication, WebApplicationContext], loader = SpringApplicationContextLoader)
@WebAppConfiguration
abstract class WebTestSpec extends TestSpec {

	@Autowired
	WebApplicationContext webApplicationContext

	@Autowired
	FilterChainProxy filterChainProxy

	@Autowired
	AuthenticationManager authenticationManager

	protected MockMvc mockMvc

	def setup() {
		createMockMvc()
	}

	/**
	 * Create a mockMvc with configured SpringSecurity
	 * @return MockMvc
	 */
	protected MockMvc createMockMvc() {
		// add Spring-Security configuration and make mockMvc use it
		mockMvc = webAppContextSetup(webApplicationContext).addFilter(filterChainProxy).build()
	}

	/**
	 * Create a MockHttpSession authorized as admin
	 * @return MockHttpSession with admin credentials
	 */
	protected MockHttpSession admin() {
		return createAuthorizedMockHttpSession('admin', 'password')
	}

	/**
	 * Create a MockHttpSession authorized as manager
	 * @return MockHttpSession with manager credentials
	 */
	protected MockHttpSession manager() {
		return createAuthorizedMockHttpSession('manager', 'password')
	}

	/**
	 * Create a MockHttpSession authorized as manager
	 * @return MockHttpSession with manager credentials
	 */
	protected MockHttpSession user() {
		return createAuthorizedMockHttpSession('user', 'password')
	}

	/**
	 * Create a MockHttpSession authorized as arbitrary user
	 * @return MockHttpSession with credentials
	 */
	protected MockHttpSession authenticate(String username, String password) {
		return createAuthorizedMockHttpSession(username, password)
	}

	/**
	 * Create a MockHttpSession that can be used with mockMvc to authenticate as a specific user
	 * @return MockHttpSession
	 */
	private createAuthorizedMockHttpSession(String username, String password) {
		Authentication authRequest = new UsernamePasswordAuthenticationToken(username, password);
		SecurityContext securityContext = SecurityContextHolder.context
		securityContext.authentication = authenticationManager.authenticate(authRequest)
		MockHttpSession httpSession = new MockHttpSession()
		httpSession.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext)
		return httpSession
	}
}
